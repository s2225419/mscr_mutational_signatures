All code used for MScR analysis, including copy number (CN) signature quantification and extraction, using CINSignatureQuantification (Drews et al, 2022) and SigFit (Gori et al, 2020).

Scripts included and how to use them:

**CONDA ENVIRONMENT YML**

_These scripts are contained in the 'ascat' directory._

ascatEnv.yml - creates conda environment to run ASCAT scripts in Eddie. 

install_ascat.R - installs ASCAT package to R from GitHub.

--------------------------------------------------------------------------

Run `conda env create -f ascatEnv.yml` to create environment.

Run `conda activate ascat` to open environment.

Open R within the ASCAT environment and run 'install_ascat.R' script.

ASCAT should now be installed to the environment. This step does **NOT** need to be repeated when using the ASCAT environment in future.

--------------------------------------------------------------------------

**ASCAT COPY NUMBER CALLING:**

_These scripts are contained in the 'ascat' directory._

mkdir.sh - creates a separate directory for each sample

prep_ascat_inR.R - takes tumour and matched normal BAM files for each sample, plus allele and loci data, and produces logR and BAF files needed for ASCAT CN calling.

tidy_ascat_inR.R - takes the logR and BAF files for each sample, filters the data to SNP6.0 postions only and tidies them into the correct format for ASCAT CN calling.  

run_ascat_inR.R - takes the filtered and formatted logR and BAF files and processes them with ASCAT. Key output from this process is the '*.segments_raw.txt' file, as this is the input to Drews' CINSignatureQuantification package (raw format used as values are unrounded, as recommended by Drews et al).

submit_ascat_inR.sh - batch script used to run jobs in parallel on EDDIE (_includes_ mkdir.sh, prep_ascat_inR.R, tidy_ascat_inR.R and run_ascat_inR.R).

bam_files_example.txt - example format for list of bam files needed when running in EDDIE (see below for format).

**BAM Files List Format (e.g. bam_files_example.txt)**

_NB: table does NOT require a header_

| sample_name | NormalBlood | PrimaryTumour | AOCS.normal | AOCS.tumor |

| sample_name | N | T | NA | NA |

Where:

sample = sample name

normal = normal label (e.g. 'N', 'NormalBlood')

tumour = tumour label (e.g. 'T', 'PrimaryTumour')

normal_id = additional normal sample identifier, if required (e.g. 'AOCS.normal'). Use NA if not required.

tumour_id = additional tumour sample identifier, if required (e.g. 'AOCS.tumor'). Use NA if not required.


**TIDY ASCAT DATA**

_These scripts are contained in the 'ascat' directory._

The signature quantification script takes a text file containing all ASCAT copy number calls for all samples. The raw segments files for each sample must first be collated into a 'segment_files' directory. This can be achieved by running the following two lines of code, which copies the raw segments file from each sample's directory into a newly created 'segment_files' directory:

`mkdir segment_files`

`find . -name '*_S1.segments_raw.txt' -exec cp {} ./segment_files/ \;`

The following scripts will format the individual segment files into a single, correctly-formatted text file for inputting to the signature quantification script:

tidy_segments_inR.R - takes individual segment files, tidies and reformats them, then concatenates them into a single text file.

submit_tidy_segments_inR.sh - script to run the 'tidy_segments_inR.R' script in EDDIE.



**SIGNATURE QUANTIFICATION:**

_These scripts are contained in the 'signature_quantification_and_extraction' directory._

signature_quantification.R - this script quantifies the existing Drews et al (2022) CN signatures from filtered ASCAT CN calls.

submit_cnsig_quant_inR.sh - script to run the 'signature_quantification.R' script in EDDIE (requires a 'file_ids.txt' file similar to 'example_bam_files.txt' file. Required columns indicated by $PARAMETER flags within the script).



**SIGNATURE EXTRACTION:**

_These scripts are contained in the 'signature_quantification_and_extraction' directory._

signature_extraction.R - takes the SxC matrix and extracts _de novo_ signatures with SigFit (Gori et al, 2020).

submit_cnsig_extract_inR.sh - script to run the 'signature_extraction.R' script in EDDIE (requires a 'file_ids.txt' file similar to 'example_bam_files.txt' file. Required columns indicated by $PARAMETER flags within the script).

signature_refit_plot.R - refits the de novo signatures obtained from the 'signature_extraction.R' script to the SxC matrix. $NSIGS parameter determined by optimal number of signatures as indicated by goodness of fit plot output from the 'signature_extraction.R' script. 

submit_cnsig_refit_inR.sh - script to run the 'signature_refit_plot.R' script in EDDIE (requires a 'file_ids.txt' file similar to 'example_bam_files.txt' file. Required columns indicated by $PARAMETER flags within the script).


**SIGNATURE ANALYSIS:**

The .Rmd scripts reproduce the analyses for the MScR project. 
Each script has a short descriptor at the start and any figures/tables produced by the script are indicated in the descriptor.
