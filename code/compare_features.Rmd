---
title: "compare_features"
output: html_notebook
---

This script calculates the features from the HGSOC cohort data WGS and SNP6 data, using the Drews et al (2022) CINSignatureQuantification package (Figure 25). 

The feature sets between the two are compared to identify which features are over or underrepresented in the SNP6 data.

# Load packages

```{r}

library(here)
library(tidyverse)
library(magrittr)
library(CINSignatureQuantification)

```

# Load data

```{r}

snp_cn <- read.table(here::here("data", "ascat_CN_hg38.txt"), header = TRUE, sep = ",", quote = " ")

wgs_cn <- read.table(here::here("data", "purple_CN.txt"), header = TRUE, sep = ",", quote = " ")

```

# Create sample by component matrix

First, a CNQuant object is built. Default genome build is 'hg19' so 'hg38' must be specified with the 'build' argument (failure to change the default build prevents calculation of features in the next step).

```{r}

snp_quant <- createCNQuant(snp_cn, build = "hg38")

wgs_quant <- createCNQuant(wgs_cn, build = "hg38")

```

Features are calculated from the CNQuant object.

```{r}

snp_features <- calculateFeatures(snp_quant, method = "drews")

wgs_features <- calculateFeatures(wgs_quant, method = "drews")

```

Build a SxC matrix from the calculated features.

```{r}

snp_sxcmatrix <- calculateSampleByComponentMatrix(snp_features, method = "drews")

wgs_sxcmatrix <- calculateSampleByComponentMatrix(wgs_features, method = "drews")

```

```{r}

snp_sxcmatrix <- snp_sxcmatrix@featFitting$sampleByComponent

wgs_sxcmatrix <- wgs_sxcmatrix@featFitting$sampleByComponent

```

# Extract feature counts as vectors

```{r}

feat <- snp_sxcmatrix %>% 
  colnames()

```

Convert the matrices to data frames for vectorisation of feature counts.

```{r}

snp_df <- data.frame(snp_sxcmatrix) %>% 
  rownames_to_column("sample")

wgs_df <- data.frame(wgs_sxcmatrix) %>% 
  rownames_to_column("sample")

snp_df <- snp_df %>% 
  dplyr::filter(sample %in% wgs_df$sample)

```

```{r}

cosine_list <- list() 

for (i in feat) {
  
  x <- snp_df %>% pull(i)
  
  y <- wgs_df %>% pull(i)
  
  cosine_list[i] <- cosine(x, y)

}

cosine <- as.data.frame(cosine_list)

```

```{r}

#write.table(cosine_list, here::here("output", "WGS_feat_cosine.txt"))

```

# Subtract SNP6 matrix from WGS matrix to see where features are under/over represented.

```{r}

feat_order <- c("segsize1", "segsize2", "segsize3", "segsize4", "segsize5", "segsize6", "segsize7", "segsize8", "segsize9", "segsize10", "segsize11", "segsize12", "segsize13", "segsize14", "segsize15", "segsize16", "segsize17", "segsize18", "segsize19", "segsize20", "segsize21", "segsize22", "changepoint1", "changepoint2", "changepoint3", "changepoint4", "changepoint5", "changepoint6", "changepoint7", "changepoint8", "changepoint9", "changepoint10", "bp10MB1", "bp10MB2", "bp10MB3", "bpchrarm1", "bpchrarm2", "bpchrarm3", "bpchrarm4", "bpchrarm5", "osCN1", "osCN2", "osCN3")

diff_df <- data.frame(
  as.matrix(snp_df %>% remove_rownames %>% column_to_rownames("sample")) - as.matrix(wgs_df %>% remove_rownames %>% column_to_rownames("sample")))

diff_sum <- as.data.frame(colSums(diff_df))

diff_sum <- diff_sum %>% 
  rownames_to_column("Feature") %>% 
  dplyr::rename(Count = "colSums(diff_df)")

diff_sum$Feature <- factor(x = diff_sum$Feature,
                               levels = feat_order,
                               ordered = TRUE)

diff_sum %>% 
  dplyr::mutate(diff = case_when(Count > 10000 ~ "Highest",
                                 Count > 5000 & Count < 10000 ~ "Higher",
                                 Count > 1000 & Count < 5000 ~ "High",
                                 Count < 1000 & Count > (-1000) ~ "Stable",
                                 Count < (-1000) & Count > (-5000) ~ "Low",
                                 Count < (-5000) & Count > (-10000) ~ "Lower",
                                 Count < (-10000) ~ "Lowest",
                                 
                                 )) %>% 
  ggplot(aes(Feature, Count, colour = diff)) +
  geom_point() +
  scale_colour_manual(values = c("#FEB1E7", "#FE01B1", "#88D4FF", "#008DDC", "#00436A", "#00BBA175")) +
  theme_classic() +
  geom_abline(yintercept = 0) +
  geom_hline(yintercept = c(1000, -1000), linetype = "dotted", size = 0.5, colour = "darkgrey") +
  geom_hline(yintercept = c(5000, -5000), linetype = "dashed", size = 0.5, colour = "darkgrey") +
  geom_hline(yintercept = c(10000, -10000), linetype = "dotdash", size = 0.5, colour = "darkgrey") +
  theme(axis.text.x = element_text(size = 8, vjust = 0.5, angle = 90),
        axis.text.y = element_text(size = 8, vjust = 0.5),
        legend.position = "none")

?scale_fill_manual
```

```{r}

#ggsave(here::here("output", "denovo_feat_comparison.png"), width = 15, height = 8, units = "cm")


```


