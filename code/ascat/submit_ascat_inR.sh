#!/bin/bash

# qsub -t 1-n submit_ascat_inR.sh bam_files_[NAME].txt
#
# IDS is the list of ids to run the R script on
#
#$ -N goAscat
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/R/3.5.1
module load igmm/bac/allelecounter

eval "$('/exports/applications/apps/SL7/anaconda/5.3.1/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

conda activate ascat

IDS=$1

SAMPLE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`
NORMAL=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $2 }'`
TUMOUR=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $3 }'`
NORMALID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $4 }'`
TUMOURID=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $5 }'`
BAM_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $6 }'`
SCRATCH_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $7 }'`
REF_FILE_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $8 }'`
BUILD=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $9 }'`
DATA_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $10 }'`
OUTPUT_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $11 }'`

bash mkdir.sh $SAMPLE

Rscript prep_ascat_inR.R $SAMPLE $NORMAL $TUMOUR $NORMALID $TUMOURID $BAM_PATH $SCRATCH_PATH $REF_FILE_PATH $BUILD
Rscript tidy_ascat_inR.R $SAMPLE $NORMAL $TUMOUR $SCRATCH_PATH $REF_FILE_PATH $DATA_PATH
Rscript run_ascat_inR.R $SAMPLE $DATA_PATH $BUILD $SCRATCH_PATH $REF_FILE_PATH $OUTPUT_PATH

conda deactivate
