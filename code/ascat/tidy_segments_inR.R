library(here)
library(tidyverse)
library(magrittr)

args <- commandArgs(trailingOnly = TRUE)

output_path <- as.character(args[1])

header <- c("chromosome", "start", "end", "segVal", "sample")
segment <- data.frame(matrix(nrow = 0, ncol = length(header))) 
colnames(segment) <- header

files <- list.files(path = paste0(output_path, "segment_files"), pattern = "*_S1.segments_raw.txt", all.files = FALSE,
           full.names = FALSE, recursive = FALSE,
           ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

for (i in files) {
  
  x <- read.table(paste0(output_path, "segment_files", i), header = TRUE) %>% 
    dplyr::select(-c(nMajor, nMinor)) %>% 
    dplyr::mutate(segVal = nAraw + nBraw) %>% 
    dplyr::mutate(segVal = round(segVal, 4)) %>% 
    dplyr::select(-c(nAraw, nBraw)) %>% 
    dplyr::relocate(sample, .after = everything()) %>% 
    dplyr::rename(chromosome = chr, start = startpos, end = endpos)
  
  x$sample <- x$sample %>% str_remove_all("_S1")
  
  segment <- rbind(segment, x)

}

write.table(segment, paste0(output_path, "ascat_CN_hg38.txt"), quote = FALSE, sep = ",")

