#!/bin/bash

# qsub submit_tidy_segments_inR.sh output_path.txt
#
#$ -N segmentTidy
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -l h_rt=96:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/R/3.5.1

eval "$('/exports/applications/apps/SL7/anaconda/5.3.1/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

conda activate ascat

IDS=$1

OUTPUT_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`

Rscript tidy_segments_inR.R $OUTPUT_PATH

conda deactivate
