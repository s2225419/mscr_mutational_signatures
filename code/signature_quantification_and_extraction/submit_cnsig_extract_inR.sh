#!/bin/bash

# To run this script, do
# qsub -t 1:n submit_cnsig_extract_inR.sh file_ids_extract.txt
#
# IDS is the list of ids to run the R script on
#
#$ -N sigExtract
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/R/4.1.3

eval "$('/exports/applications/apps/SL7/anaconda/5.3.1/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

conda activate sigExtract

IDS=$1
FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`
NSIGS1=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $2 }'`
NSIGS2=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $3 }'`
WARMUP=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $4 }'`
ITER=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $5 }'`
OUTPATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $6 }'`
TYPE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $7 }'`

Rscript signature_extraction.R $FILE $NSIGS1 $NSIGS2 $WARMUP $ITER $OUTPATH $TYPE

conda deactivate
