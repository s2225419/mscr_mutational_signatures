#' ---
#' title: "signature_extraction"
#' output: html_notebook
#' ---
#' 
#' This script extracts de-novo structural variant (SV) signatures from the HGSOC cohort data, using the *SigFit* package, Gori et al (2018).
#' 
#' # Install packages
#' 
## ---------------------------------------------------------------------------------------------------------------------------------------

#devtools::install_github("kgori/sigfit", build_vignettes = TRUE, build_opts = c("--no-resave-data", "--no-manual"))

#' 
#' # Load packages
#' 
## ---------------------------------------------------------------------------------------------------------------------------------------

library(here)
library(tidyverse)
library(magrittr)
library(sigfit)

#' 
#' # Supply command line arguments
#' 
## ---------------------------------------------------------------------------------------------------------------------------------------

args <- commandArgs(trailingOnly = TRUE)

file <- as.character(args[1])

nsigs_1 <- as.numeric(args[2])

nsigs_2 <- as.numeric(args[3])

warmup <- as.numeric(args[4])

iter <- as.numeric(args[5])

output_path <- as.character(args[6])

type <- as.character(args[7])

#'
#' # Load SxC matrix
#'
## ---------------------------------------------------------------------------------------------------------------------------------------

matrix <- read.table(here::here(file), header = TRUE, sep = "", quote = "")

#'
#' # Plot mutation spectra for all samples
#'
## ---------------------------------------------------------------------------------------------------------------------------------------

plot_spectrum(matrix, pdf_path = here::here(output_path, paste0(type, "_CNcounts_spectra.pdf")))

#' 
#' # Extract signatures
#' 
## ---------------------------------------------------------------------------------------------------------------------------------------


HGSOC_sigs_denovo <- extract_signatures(counts = matrix,
                                              nsignatures = nsigs_1:nsigs_2,
                                              iter = iter, 
                                              warmup = warmup,
                                              seed = 2222,
                                              control = list(adapt_delta = 0.99))

#' 
#' # Plot goodness-of-fit model and save as PDF 
#' 
#' Determine optimal number of signatures with GOF model
#' 
## ---------------------------------------------------------------------------------------------------------------------------------------

pdf(file = paste0(output_path, type, "_cnsigs_gof_", nsigs_1, "_to_", nsigs_2, ".pdf"))

plot_gof(HGSOC_sigs_denovo)

dev.off()

