#!/bin/bash

# To run this script, do
# qsub -t 1:n submit_cnsigsquant_inR.sh quant_paths.txt

#$ -N quantCNSigs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=16G
#$ -l h_rt=24:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/R/4.1.3

eval "$('/exports/applications/apps/SL7/anaconda/5.3.1/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

conda activate cnsigs

IDS=$1
FILE_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`
BUILD=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $2 }'`
OUTPUT_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $3 }'`
COHORT_DATA_PATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $4 }'`

Rscript signature_quantification.R $FILE_PATH $BUILD
Rscript signature_plotting.R $FILE_PATH $BUILD $OUTPUT_PATH $COHORT_DATA_PATH
Rscript signature_clustering.R $FILE_PATH $BUILD $OUTPUT_PATH $COHORT_DATA_PATH

conda deactivate
