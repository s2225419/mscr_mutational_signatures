#!/bin/bash

# To run this script, do
# qsub -t 1:n submit_cnsig_refit_inR.sh file_ids_refit.txt
#
# IDS is the list of ids to run the R script on
#
#$ -N refitCNSigs
#$ -j y
#$ -S /bin/bash
#$ -cwd
#$ -l h_vmem=64G
#$ -l h_rt=48:00:00

unset MODULEPATH
. /etc/profile.d/modules.sh

module load igmm/apps/R/4.1.3

eval "$('/exports/applications/apps/SL7/anaconda/5.3.1/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"

conda activate sigExtract

IDS=$1
FILE=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $1 }'`
NSIGS=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $2 }'`
WARMUP=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $3 }'`
ITER=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $4 }'`
OUTPATH=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $5 }'`
DATA=`head -n $SGE_TASK_ID $IDS | tail -n 1 | awk '{ print $6 }'`

Rscript signature_refit_plot.R $FILE $NSIGS $WARMUP $ITER $OUTPATH $DATA

conda deactivate

