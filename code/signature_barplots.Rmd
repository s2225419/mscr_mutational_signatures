---
title: "signature_barplots"
output: html_notebook
---

This script plots per-sample stacked bar plots of proportional CN signature activities (Figures 9 and 31). 

# Load packages

```{r}

library(here)
library(tidyverse)
library(magrittr)
library(gplots)
library(RColorBrewer)
library(cluster)
library(factoextra)

```

# Load data

```{r}

activity <- read.table(here::here("data", "hgsoc_cohort_data.txt"), quote = "") %>% 
  dplyr::filter(cellularity >= 0.4)

```

# Choose signatures of interest

Choose appropriate prefix for signatures of interest: "CX" = Drews signatures fit to SNP6.0 data, "WGS_CN" = de novo signatures, "WGS_CX" = Drews signatures fit to WGS data.

```{r}

prefix <- as.character("WGS_CN")

```

# Create custom palette for 17 signatures

Need to create custom palette to provide additional colours for each of the 17 signatures in the data (Drews sigs only).

```{r}

coul <- brewer.pal(12, "Set3") 

coul <- colorRampPalette(coul)(17)

```

# Pivot data for plotting

```{r}

activity_long <- activity %>%
  dplyr::select(cohort, sample, starts_with(prefix)) %>% 
  drop_na() %>% 
  pivot_longer(cols = !c(cohort, sample), names_to = "CN_sig", values_to = "activity") 

```

# Plot stacked bar graph of CN signatures (proportion) per sample

Bar plot to show the relative frequency of CN signature activity per sample.

NB: Use 'scale_fill_manual(values = coul)' when plotting Drews sigs to provide 17 colours. 
Use 'scale_fill_brewer(palette = "Set3")' when plotting de novo sigs.

```{r}

activity_long %>%
  ggplot(aes(sample, activity, fill = CN_sig)) + 
  geom_bar(position = "fill", stat = "identity") +
 # scale_fill_manual(values = coul) +
  scale_fill_brewer(palette = "Set3") +
  xlab("Sample") +
  ylab("Relative frequency of signatures") +
  labs(fill = "Copy Number \n Signature") +
  guides(fill = guide_legend(ncol = 2)) +
  theme_minimal() +
  theme(legend.position = "top") +
  theme(legend.title = element_text(size = 18)) +
  theme(legend.text = element_text(size = 18)) +
  theme(axis.title.x = element_text(size = 24),
        axis.title.y = element_text(size = 24, vjust = 2),
        axis.text.x = element_blank(),
        axis.text.y = element_text(size = 18, vjust = 0.5))

```

```{r}

#ggsave(paste0(prefix, "_stacked.pdf"), path = here ::here("output"), width = 16, height = 8)

```


