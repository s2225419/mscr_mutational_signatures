---
title: "cluster_comparison_boxplots"
output: html_notebook
---

This script makes a boxplot which compares signature activity between clusters of Drews or de novo signature activity (Figures 14 and 35). 
The script also calculates the proportion of each signature in the cohort (Tables 3 and 8). 

# Load packages

```{r}

library(here)
library(magrittr)
library(tidyverse)
library(RColorBrewer)

```

# Add colour-blind friendly palette for plots

```{r}

coul <- brewer.pal(12, "Set3") 

coul <- colorRampPalette(coul)(17)

```

### DREWS

# Load data

```{r}

drews <- read.table(here::here("data", "drews_sigs_SNP.txt"), sep = ",", header = TRUE) %>% 
  rownames_to_column(var = "sample")

drews_clus <- read.table(here::here("output", "CX_samples_in_hcluster.txt"), header = TRUE)

drews <- left_join(drews_clus, drews)

drews <- drews %>% 
  pivot_longer(cols = CX01:CX17, names_to = "sig", values_to = "activity")

```


# Box plot activity by cluster

```{r}

drews %>% 
  ggplot(aes(x = sig, y = activity, fill = sig)) +
  geom_boxplot() +
  scale_fill_manual(values = coul) +
  facet_wrap(~ cluster) +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.title.align = 0.5) +
  theme(axis.title.x = element_text(size = 10),
        axis.title.y = element_text(size = 10),
        axis.text.x = element_text(size = 8, angle = 90, vjust = 0.5),
        axis.text.y = element_text(size = 8, vjust = 0.5),
        legend.position = "none") +
  labs(x = "Copy Number Signature (Drews)", y = "Activity")

```

```{r}

#ggsave(here::here("output", "drews_sig_boxplot.pdf"), width = 15, height = 8, units = "cm")

```

# Calculate ratio of signatures between clusters

```{r}

clus_1 <- drews %>% 
  pivot_wider(id_cols = c(sample, cluster), names_from = "sig", values_from = "activity") %>%
  dplyr::select(-sample) %>% 
  dplyr::filter(cluster == 1) %>%
  colSums() %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "signature") %>% 
  dplyr::filter(signature != "cluster") %>% 
  dplyr::rename(total = ".") %>% 
  dplyr::mutate(total = total/141) %>% 
  dplyr::mutate(clus_1 = round(total, 2)) %>% 
  dplyr::select(-total) 

clus_2 <- drews %>% 
  pivot_wider(id_cols = c(sample, cluster), names_from = "sig", values_from = "activity") %>%
  dplyr::select(-sample) %>% 
  dplyr::filter(cluster == 2) %>%
  colSums() %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "signature") %>% 
  dplyr::filter(signature != "cluster") %>% 
  dplyr::rename(total = ".") %>% 
  dplyr::mutate(total = total/127) %>% 
  dplyr::mutate(clus_2 = round(total, 2)) %>% 
  dplyr::select(-total)

drews_clus_prop <- full_join(clus_1, clus_2) %>% 
  dplyr::mutate(diff = clus_1 - clus_2)

drews_clus_prop %>% 
  dplyr::select(-signature) %>% 
  colSums()

```

# Save proportion data

```{r}

write.table(drews_clus_prop, here::here("output", "drews_clus_prop.txt"), quote = FALSE, row.names = FALSE)

```


### DE NOVO

# Load data

```{r}

denovo <- read.table(here::here("data", "denovo_sigs_WGS.txt"), sep = ",", header = TRUE) %>% 
  dplyr::rename(WGS_CN_A = Signature.A, WGS_CN_B = Signature.B, WGS_CN_C = Signature.C) %>% 
  rownames_to_column("sample")

denovo_clus <- read.table(here::here("output", "WGS_CN_samples_in_hcluster.txt"), header = TRUE)

denovo <- left_join(denovo_clus, denovo)

denovo <- denovo %>% 
  pivot_longer(cols = WGS_CN_A:WGS_CN_C, names_to = "sig", values_to = "activity")

```

# Box plot activity by cluster

```{r}

denovo %>% 
  ggplot(aes(x = sig, y = activity, fill = sig)) +
  geom_boxplot() +
  scale_fill_brewer(palette = "Set3") +
  facet_wrap(~ cluster) +
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.title.align = 0.5) +
  theme(axis.title.x = element_text(size = 10),
        axis.title.y = element_text(size = 10),
        axis.text.x = element_text(size = 8, vjust = 0.5),
        axis.text.y = element_text(size = 8, vjust = 0.5),
        legend.position = "none") +
  labs(x = "Copy Number Signature (De Novo)", y = "Activity")

```

```{r}

#ggsave(here::here("output", "denovo_sig_boxplot.pdf"), width = 15, height = 8, units = "cm")

```

# Calculate ratio of signatures between clusters

```{r}

dn_clus_1 <- denovo %>% 
  pivot_wider(id_cols = c(sample, cluster), names_from = "sig", values_from = "activity") %>%
  dplyr::select(-sample) %>% 
  dplyr::filter(cluster == 1) %>% 
  colSums() %>%
  as.data.frame() %>%
  rownames_to_column(var = "signature") %>%
  dplyr::filter(signature != "cluster") %>%
  dplyr::rename(total = ".") %>%
  dplyr::mutate(total = total/177) %>% 
  dplyr::mutate(clus_1 = round(total, 2)) %>%
  dplyr::select(-total)

dn_clus_2 <- denovo %>% 
  pivot_wider(id_cols = c(sample, cluster), names_from = "sig", values_from = "activity") %>%
  dplyr::select(-sample) %>% 
  dplyr::filter(cluster == 2) %>%
  colSums() %>% 
  as.data.frame() %>% 
  rownames_to_column(var = "signature") %>% 
  dplyr::filter(signature != "cluster") %>% 
  dplyr::rename(total = ".") %>%
  dplyr::mutate(total = total/89) %>% 
  dplyr::mutate(clus_2 = round(total, 2)) %>% 
  dplyr::select(-total)

denovo_clus_prop <- full_join(dn_clus_1, dn_clus_2) %>% 
  dplyr::mutate(diff = clus_1 - clus_2)

denovo_clus_prop %>% 
  dplyr::select(-signature) %>% 
  colSums()

```

# Save proportion data

```{r}

write.table(denovo_clus_prop, here::here("output", "denovo_clus_prop.txt"), quote = FALSE, row.names = FALSE)

```



