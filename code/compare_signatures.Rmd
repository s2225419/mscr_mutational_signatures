---
title: "compare_signatures"
output: html_notebook
---

This script compares copy number signatures quantified in WGS data and SNP6 data using cosine similarity (Tables 7 and 11). 

# Load packages

```{r}

library(here)
library(tidyverse)
library(magrittr)
library(lsa)
library(sigfit)

```

# Specify data type ("drews" or "denovo")

```{r}

type <- "denovo"

```

# Load data

Let's load the cohort data, filter out the samples with >40% cellularity and then select only the columns containing CN signature activity.

```{r}

`%nin%` = Negate(`%in%`)

cell <- read.table(here::here("data", "hgsoc_cohort_data.txt"), header = TRUE, sep = " ", quote = "") %>% 
  dplyr::filter(cellularity >= 0.4) %>% 
  pull(sample)

WGS <- read.table(here::here("data", paste0(type, "_sigs_WGS.txt")), header = TRUE, sep = ",", quote = "") %>%
  rownames_to_column("sample") %>% 
  dplyr::filter(sample %in% cell)

SNP <- read.table(here::here("data", paste0(type, "_sigs_SNP.txt")), header = TRUE, sep = ",", quote = "")  %>% 
  rownames_to_column("sample") %>% 
  dplyr::filter(sample %in% cell) 

a <- SNP %>% 
  dplyr::filter(sample %nin% WGS$sample)

SNP <- SNP %>% 
  dplyr::filter(sample %in% WGS$sample)
  
```

# Extract signature deconvolutions as vectors

```{r}

signature <- SNP %>% 
  dplyr::select(!sample) %>% 
  colnames()

```

```{r}

cosine_list <- list() 

for (i in signature) {
  
  x <- SNP %>% pull(i)
  
  y <- WGS %>% pull(i)
  
  cosine_list[i] <- cosine(x, y)

}

cosine <- as.data.frame(cosine_list)

```

```{r}

#write.table(cosine_list, here::here("output", paste0(type, "_sig_cosine.txt")))

```

# Compare Drews SNP-derived signatures to de novo WGS derived signatures

# Load data

Let's load the cohort data, filter out the samples with >40% cellularity and then select only the columns containing CN signature activity.

```{r}

`%nin%` = Negate(`%in%`)

cell <- read.table(here::here("data", "hgsoc_cohort_data.txt"), header = TRUE, sep = " ", quote = "") %>% 
  dplyr::filter(cellularity >= 0.4) %>% 
  pull(sample)

drews <- read.table(here::here("data", "drews_sigs_SNP.txt"), header = TRUE, sep = ",", quote = "") %>%
  rownames_to_column("sample") %>% 
  dplyr::filter(sample %in% cell)

denovo <- read.table(here::here("data", "denovo_sigs_WGS.txt"), header = TRUE, sep = ",", quote = "")  %>% 
  rownames_to_column("sample") %>% 
  dplyr::filter(sample %in% cell) 

a <- drews %>% 
  dplyr::filter(sample %nin% denovo$sample)

drews <- drews %>%
  dplyr::filter(sample %in% denovo$sample)
  
```

# Extract signature deconvolutions as vectors

```{r}

drews_sig <- drews %>% 
  dplyr::select(!sample) %>% 
  colnames()

denovo_sig <- denovo %>% 
  dplyr::select(!sample) %>% 
  colnames()

```

```{r}

cosine_list_A <- list() 

for (i in drews_sig) {
  
  x <- drews %>% pull(i)
  
  y <- denovo$Signature.A
  
  cosine_list_A[i] <- cosine(x, y)

  }

cosine_A <- as.data.frame(cosine_list_A)

```

```{r}

cosine_list_B <- list() 

for (i in drews_sig) {
  
  x <- drews %>% pull(i)
  
  y <- denovo$Signature.B
  
  cosine_list_B[i] <- cosine(x, y)

  }

cosine_B <- as.data.frame(cosine_list_B)

```

```{r}

cosine_list_C <- list() 

for (i in drews_sig) {
  
  x <- drews %>% pull(i)
  
  y <- denovo$Signature.C
  
  cosine_list_C[i] <- cosine(x, y)

  }

cosine_C <- as.data.frame(cosine_list_C)

```

```{r}

#write.table(cosine_list, here::here("output", "both_sig_cosine.txt"))

```
